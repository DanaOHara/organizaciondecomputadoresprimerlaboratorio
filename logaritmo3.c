#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>


double logaritmo (double num) {
    double k = (((num)-1)/((num)+1));
	double k2 = k*k;
	double kacum = 0;
	double res = 0;

	// Iteración 3
	// Se cargan las constantes directamente en el código y se dejan explícitas las variables en las sumas y multiplicaciones
	kacum = k;
	res = kacum;
	kacum = kacum * k2;
	res = res + (kacum * 0.3333333333333333333333);
	kacum = kacum * k2;
	res = res + (kacum * 0.2);
	kacum = kacum * k2;
	res = res + (kacum * 0.1428571428571428571429);
	kacum = kacum * k2;
	res = res + (kacum * 0.1111111111111111111111);
	kacum = kacum * k2;
	res = res + (kacum * 0.09090909090909090909091);
	kacum = kacum * k2;
	res = res + (kacum * 0.07692307692307692307692);
	kacum = kacum * k2;
	res = res + (kacum * 0.06666666666666666666667);
	kacum = kacum * k2;
	res = res + (kacum * 0.0588235294117647058823);
	kacum = kacum * k2;
	res = res + (kacum * 0.05263157894736842105263);
	kacum = kacum * k2;
	res = res + (kacum * 0.04761904761904761904762);
	kacum = kacum * k2;
	res = res + (kacum * 0.0434782608695652173913);
	kacum = kacum * k2;
	res = res + (kacum * 0.04);
	kacum = kacum * k2;
	res = res + (kacum * 0.03703703703703703703704);
	kacum = kacum * k2;
	res = res + (kacum * 0.03448275862068965517241);

	return res * 2;
}


int main (int argc, char **argv) {
	int dec = 15;	// Número de decimales a mostrar
	int iflag = 0;	// Flag de parámetro -i
	int nflag = 0;	// Flag de parámetro -n
	int i, c;		// Constantes Auxiliares
	
	int iter = 0;		// Número de iteraciones que se realizan para el profiler
	double num = 0;		// Número a calcular el Logaritmo Natural

	// Inicio GETOPT
	while ((c = getopt (argc, argv, "i:n:")) != -1) {
		switch (c) {
			case 'i':
				iflag = 1;			// Argumento -i (cantidad de ejecuciones)
				iter = atoi(optarg);
				break;
			case 'n':				// Argumento -n (número a calcular)
				nflag = 1;
				num = atof(optarg);
				break;
			case '?':				// Otro argumento
				break;
			default:				// Si se ingresan argumentos erróneos, hay error
				printf("ERROR: Argumentos Incorrectamente Ingresados\n");
				return 1;
		  }
	}
	// Fin GETOPT

	if (iflag == 1 && nflag == 1) {		// Verifica si se han ingresado ambos argumentos
		if (num > 0 && iter >= 0) {		// Verifica si los valores de los argumentos son válidos
			double resultado;			// Acumulador del resultado

			for (i=0; i<iter; i++) {			// Se itera el logaritmo las veces indicadas
				resultado = logaritmo(num);
			}

			printf("%.*f\n", dec, resultado);	// Se imprime por pantalla el resultado con los decimales indicados
			return 0;
		}
		else {								// Si los valores del argumento son inválidos, hay error
			printf("ERROR: Valores del Argumento Invalidos\n");
			return 1;
		}
	}
	else {									// Si los argumentos no son suficientes, hay error
		printf("ERROR: Argumentos Insuficientes\n");
		return 1;
	}
}
