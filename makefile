# MAKEFILE
# Lab1 Orga
# Daniel Gacitúa y Rosa Muñoz

SOURCES = logaritmo0 logaritmo1 logaritmo2 logaritmo3 logaritmo4 logaritmo5

all: $(SOURCES)

logaritmo0: logaritmo0.c
	gcc -pg -O0 -std=c99 logaritmo0.c -o logaritmo0

logaritmo1: logaritmo1.c
	gcc -pg -O0 -std=c99 logaritmo1.c -o logaritmo1

logaritmo2: logaritmo2.c
	gcc -pg -O0 -std=c99 logaritmo2.c -o logaritmo2

logaritmo3: logaritmo3.c
	gcc -pg -O0 -std=c99 logaritmo3.c -o logaritmo3
	
logaritmo4: logaritmo4.c
	gcc -pg -O0 -std=c99 logaritmo4.c -o logaritmo4
	
logaritmo5: logaritmo5.c
	gcc -pg -O0 -std=c99 logaritmo5.c -o logaritmo5

.PHONY: clean cleanall purge

clean:
	rm -f $(SOURCES).o

cleanall:
	rm -f $(SOURCES) $(SOURCES).o

purge:
	rm -f $(SOURCES) $(SOURCES).o gmon.out
