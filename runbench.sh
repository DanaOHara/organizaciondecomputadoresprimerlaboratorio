#!/bin/bash

# Programa para Tests de Prueba
# Lab1 Orga
# Daniel Gacitúa y Rosa Muñoz

# Argumentos del programa
NUM=2
EJE=10000000
ITER="logaritmo5"

# Script
echo -e "\e[0;32m=> \e[1;31mLimpiando Directorio...\e[m"
make purge

echo -e "\e[0;32m=> \e[1;31mCompilando Programa [$ITER]...\e[m"
make

echo -e "\e[0;32m=> \e[1;31mEliminando Objetos...\e[m"
make clean

echo -e "\e[0;32m=> \e[1;31mLanzando Programa [ln($NUM) con $EJE ejecuciones]\e[m"
./$ITER -n $NUM -i $EJE

echo -e "\e[0;32m=> \e[1;31mMostrando Estadísticas...\e[m"
gprof ./$ITER gmon.out
