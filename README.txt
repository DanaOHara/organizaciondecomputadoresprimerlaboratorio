Laboratorio 1
Organización de Computadores 2014-2
Universidad de Santiago de Chile

Integrantes:
- Daniel Gacitúa Vásquez ('GaciX' en BitBucket)
- Rosa Muñoz Valenzuela ('DanaOHara' en BitBucket)

Dirección del Repositorio Git:
https://bitbucket.org/DanaOHara/organizaciondecomputadoresprimerlaboratorio

Dirección para Clonar:
git@bitbucket.org:DanaOHara/organizaciondecomputadoresprimerlaboratorio.git
