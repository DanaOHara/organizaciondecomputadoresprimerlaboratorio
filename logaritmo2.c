#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>


static double c0 = 1;
static double c1 = 0.3333333333333333333333;
static double c2 = 0.2;
static double c3 = 0.1428571428571428571429;
static double c4 = 0.1111111111111111111111;
static double c5 = 0.09090909090909090909091;
static double c6 = 0.07692307692307692307692;
static double c7 = 0.06666666666666666666667;
static double c8 = 0.05882352941176470588235;
static double c9 = 0.05263157894736842105263;
static double c10 = 0.04761904761904761904762;
static double c11 = 0.0434782608695652173913;
static double c12 = 0.04;
static double c13 = 0.03703703703703703703704;
static double c14 = 0.03448275862068965517241;


double logaritmo (double num) {
	double k = ((num-1)/(num+1));
	double k2 = k*k;
	double kacum = 0;
	double res = 0;
	
	// Iteración 2
	// Se incluye k2 = k * k
	kacum = k;
	res += c0 * kacum;
	kacum = kacum * k2;
	res += c1 * kacum;
	kacum = kacum * k2;
	res += c2 * kacum;
	kacum = kacum * k2;
	res += c3 * kacum;
	kacum = kacum * k2;
	res += c4 * kacum;
	kacum = kacum * k2;
	res += c5 * kacum;
	kacum = kacum * k2;
	res += c6 * kacum;
	kacum = kacum * k2;
	res += c7 * kacum;
	kacum = kacum * k2;
	res += c8 * kacum;
	kacum = kacum * k2;
	res += c9 * kacum;
	kacum = kacum * k2;
	res += c10 * kacum;
	kacum = kacum * k2;
	res += c11 * kacum;
	kacum = kacum * k2;
	res += c12 * kacum;
	kacum = kacum * k2;
	res += c13 * kacum;
	kacum = kacum * k2;
	res += c14 * kacum;
	
	return res * 2;
}


int main (int argc, char **argv) {
	int dec = 15;	// Número de decimales a mostrar
	int iflag = 0;	// Flag de parámetro -i
	int nflag = 0;	// Flag de parámetro -n
	int i, c;		// Constantes Auxiliares
	
	int iter = 0;		// Número de iteraciones que se realizan para el profiler
	double num = 0;		// Número a calcular el Logaritmo Natural

	// Inicio GETOPT
	while ((c = getopt (argc, argv, "i:n:")) != -1) {
		switch (c) {
			case 'i':
				iflag = 1;			// Argumento -i (cantidad de ejecuciones)
				iter = atoi(optarg);
				break;
			case 'n':				// Argumento -n (número a calcular)
				nflag = 1;
				num = atof(optarg);
				break;
			case '?':				// Otro argumento
				break;
			default:				// Si se ingresan argumentos erróneos, hay error
				printf("ERROR: Argumentos Incorrectamente Ingresados\n");
				return 1;
		  }
	}
	// Fin GETOPT

	if (iflag == 1 && nflag == 1) {		// Verifica si se han ingresado ambos argumentos
		if (num > 0 && iter >= 0) {		// Verifica si los valores de los argumentos son válidos
			double resultado;			// Acumulador del resultado

			for (i=0; i<iter; i++) {			// Se itera el logaritmo las veces indicadas
				resultado = logaritmo(num);
			}

			printf("%.*f\n", dec, resultado);	// Se imprime por pantalla el resultado con los decimales indicados
			return 0;
		}
		else {								// Si los valores del argumento son inválidos, hay error
			printf("ERROR: Valores del Argumento Invalidos\n");
			return 1;
		}
	}
	else {									// Si los argumentos no son suficientes, hay error
		printf("ERROR: Argumentos Insuficientes\n");
		return 1;
	}
}
